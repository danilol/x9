# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require 'version'

Gem::Specification.new do |gem|
  gem.name          = "x9"
  gem.version       = X9::VERSION
  gem.authors       = ["Core iba"]
  gem.email         = ["core@iba.com.br"]
  gem.summary       = "Gem to write log for Arcsight"
  gem.description   = gem.summary
  gem.homepage      = "https://bitbucket.org/abrilmdia/iba-x9"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_dependency "rake"

  gem.add_development_dependency "elco-utils"
  gem.add_development_dependency "geminabox"
  gem.add_development_dependency "delorean"
  gem.add_development_dependency "activesupport", "~>3.2"
end
